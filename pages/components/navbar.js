import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExport, faHome, faPhone } from '@fortawesome/free-solid-svg-icons'

export default function Navbar() {
    return (
    <div className="bg-blue-500 w-full max-h-16 shadow-xl">
        <div className="flex flex-row bg-blue-500">
          <div>
          <img
            src="/bobgang.png"
            alt="Picture of the author"
            className="w-16 h-16"
          />
          </div>
          <div className="m-5  font-semibold text-white"> 
            <Link href="/">
              <a> <FontAwesomeIcon size="1x" icon={faHome} />  Inicio</a>
            </Link>
          </div>
          <div className="m-5 font-semibold text-white">
            <Link href="/general">
              <a> <FontAwesomeIcon size="1x" icon={faPhone} />  Contacto</a>
            </Link>
          </div>
          <div className="m-5 font-semibold text-white">
          <Link href="/general">
              <a> <FontAwesomeIcon size="1x" icon={faFileExport} /> Exportar</a>
            </Link>
          </div>         
        </div>
        <div className="float-right -mt-12 mr-10 text-xl font-bold text-white">
        Personal's Finances App
        </div>        
      </div>     
    );
}