import { useState, ReactFragment } from "react";
import Navbar from "./components/navbar";
import { useForm } from "react-hook-form";
import moment from "moment";




function RenderTable(dataArray) {

  const RenderBody = () => {
    const renderRows = (rows) => rows.map(r => {
      return (
        <tr className="border-dotted border-black border-2" key={r.date}>
          <td>{moment(r.date).format('MM/DD/YYYY')}</td>
          <td className="border-dotted border-black border-2">{r.description}</td>
          <td className={r.value >= 0 ? "text-green-700": "text-red-600"} > $ {r.value}</td>
        </tr>
      );
    });

    if(dataArray.length !== 0) {
      return renderRows(dataArray);
    } else {
      return (
      <tr> 
        <td></td>
        <td>No hay ningun registro </td>
        <td></td>
      </tr>);
    }
  }

  return (
    <div>
      <table className="table mx-auto border-black min-w-3/4 shadow-2xl">
        <thead>
          <tr className="bg-blue-600 border-solid border-black border-2 h-10 text-white">
            <th>Fecha</th>
            <th className="border-solid border-black border-2">Descripción</th>
            <th>Gasto/Ingreso</th>
          </tr>
        </thead>
        <tbody>
          {RenderBody()}
        </tbody>
      </table>
    </div>
  );
}

function RenderBalance(dataArray) {

  const balance = () => {
    let value = 0;
    if(dataArray.length !== 0) {
      dataArray.forEach(r => {
        value += r.value;
      });
    }
    return (
      <div className={value >= 0 ? "text-green-700" : "text-red-700" }>
        $ {value}
      </div>
    );
  } 

  return (
    <div>
      <div className="m-5 mx-auto max-w-max shadow-2xl flex flex-row rounded ">
        <div className="m-10 text-xl">Saldo</div>
        <div className="m-10 text-xl">
          {balance()}
        </div>
      </div>
    </div>
  );
}

function RenderForm(inputArray, setArray) {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const onSubmit = data => {
    console.log(data);
    console.log(inputArray);
    setArray(
      inputArray.concat({
        value: data.type === 'INGRESS' ? parseInt(data.ingress) : -parseInt(data.ingress),
        date: new Date(),
        description: data.description
      })
    );
  };

  console.log(watch("example")); // watch input value by passing the name of it
  return (
    <div>
      <div className="m-6 mx-auto max-w-max shadow-2xl rounded ">
        <div className="text-xl p-5">
          Ingresar un nuevo Registro
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-row">
            <label className="m-5 w-1/2">Ingresa el valor</label>
            <input className="m-5 h-10 w-1/2 border-solid border-gray-500 border-2 text-center float-right" type="number" {...register("ingress", {required: true, min: 1})} /> <br/>
          </div>
          <div className="flex flex-row">
            <label className="m-5 w-1/2">Selecciona el tipo de registro</label>
            <select className="m-5 h-10 border-solid border-gray-500 border-2 w-1/2 text-center" {...register("type", {required: true})}>
              <option value="INGRESS">Ingreso</option>
              <option value="EGRESS">Egreso</option>
            </select> <br/>
          </div>
          <div className="flex flex-row">
            <label className="m-5 w-1/2">Ingresa una descripción <br /> </label> 
            <textarea className="m-5 w-1/2 h-20 border-solid border-gray-500 border-2 text-center" {...register("description", {required: true})} /> <br/>
          </div>
          <div>
            {errors.ingress && <span className="text-red-600">El valor es requerido y debe ser mayor a 0</span> }
            {errors.type && <span className="text-red-600">El valor es requerido y debe ser mayor a 0</span> }
            {errors.description && <span className="text-red-600">El valor es requerido y debe ser mayor a 0</span> }
          </div>

          <button className="bg-green-500 w-full h-10 rounded text-white" type="submit">Enviar</button>
        </form>
      </div>
    </div>
  );
}

export default function Home() {

  const [data, setData] = useState([]);

  return (
    <div>
      <div className="mb-5">
        <Navbar></Navbar>
      </div>
      <div className="bg-gray-300">
        <div className="min-w-full min-h-screen text-center bg-white">          
          {RenderBalance(data)}
          {RenderTable(data)}
          {RenderForm(data, setData)}
        </div>
      </div>
    </div>
  );
}
